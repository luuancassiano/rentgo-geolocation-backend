const mongoose = require('mongoose')

const driverLocationSchema = new mongoose.Schema({

    uuid: {
        type: String,
        required: true
    },

    latitude: {
        type: Number,
        required: true
    },

    longitude: {
        type: Number,
        required: true
    },

    createdAt: {
        type: Date,
        default: Date.now
    }
})

const driverLocation = mongoose.model('driver_location', driverLocationSchema)

module.exports = driverLocation