const DriverLocation = require('../models/DriverLocation')
const uuid = require('uuid-random')

class DriverLocationController {

    async index(req, res) {
        try {
            const drvLocs = await DriverLocation.find({
                uuid: req.params.uuid
            })

            return res.status(200).json(drvLocs)
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }

    async store(req, res) {
        try {
            const drvLoc = await DriverLocation.create({ ...req.body, uuid: uuid() })

            return res.status(201).json(drvLoc)
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }

    async update(req, res) {
        try {
            const drvLoc = await DriverLocation.findByIdAndUpdate(req.params.id, req.body, {
                new: true
            })

            return res.status(200).json(drvLoc)
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }
}

module.exports = new DriverLocationController()