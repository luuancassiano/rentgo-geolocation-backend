const express = require('express')
const Routes = express.Router()

const DriverLocationController = require('./app/controllers/DriverLocationController')

Routes.get('/api/driver-location/:uuid', DriverLocationController.index)
Routes.post('/api/driver-location', DriverLocationController.store)
Routes.put('/api/driver-location/:id', DriverLocationController.update)

module.exports = Routes